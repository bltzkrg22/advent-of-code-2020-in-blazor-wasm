﻿using System;
using System.Collections.Generic;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution09 : SolutionCommon
    {
        private const int PreambleSize = 25;

        static bool IsValid(long nextNumber, List<long> previous)
        {
            for (int i = 0; i < previous.Count; i++)
            {
                for (int j = i + 1; j < previous.Count; j++)
                {
                    if (nextNumber == previous[i] + previous[j])
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            if (inputLines.Length <= PreambleSize)
            {
                return "Input too short to find an answer!";
            }

            // Queue would be harder to iterate over - we are interested in all numbers
            // from previous 25, not only the last one. So we will use a List and some
            // dynamic programming concepts
            var previousNumbers = new List<long>();

            for (int i = 0; i < PreambleSize; i++)
            {
                previousNumbers.Add(Int64.Parse(inputLines[i]));
            }

            for (int i = PreambleSize; i < inputLines.Length; i++)
            {
                long nextNumber = Int64.Parse(inputLines[i]);
                if (!IsValid(nextNumber, previousNumbers))
                {
                    return nextNumber.ToString();
                }
                else
                {
                    int indexToReplace = i % PreambleSize;
                    previousNumbers[indexToReplace] = nextNumber;
                }
            }

            return "Answer was not found!";
        }

        public override string PartB(string input)
        {
            string stringAnswer = PartA(input);
            if (!Int64.TryParse(stringAnswer, out long cipherWeakness))
            {
                return stringAnswer;
            }

            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);


            int slidingWindowFrontIndex = 1;
            int slidingWindowBackIndex = 0;
            long slidingWindowSum = Int64.Parse(inputLines[slidingWindowFrontIndex]) +
                                    Int64.Parse(inputLines[slidingWindowBackIndex]);


            while (slidingWindowSum != cipherWeakness || slidingWindowFrontIndex == slidingWindowBackIndex)
            {
                // The sliding window must always have at least two numbers, so if
                // slidingWindowBackIndex = slidingWindowFrontIndex then we always add a number
                if (slidingWindowSum < cipherWeakness || slidingWindowFrontIndex == slidingWindowBackIndex)
                {
                    // Add the next number into sliding window
                    slidingWindowFrontIndex += 1;

                    // Defend ourselves if we traversed whole input list and the answer was not found
                    try
                    {
                        slidingWindowSum += Int64.Parse(inputLines[slidingWindowFrontIndex]);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        return "Answer was not found!";
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        return "Answer was not found!";
                    }
                }
                else // if (slidingWindowSum > cipherWeakness)
                {
                    // Remove the last number from sliding window
                    slidingWindowSum -= Int64.Parse(inputLines[slidingWindowBackIndex]);
                    slidingWindowBackIndex += 1;
                }
            }


            // Now find the min and max from the sliding window
            long slidingWindowMin = Int64.MaxValue;
            long slidingWindowMax = Int64.MinValue;

            for (int i = slidingWindowBackIndex; i <= slidingWindowFrontIndex; i++)
            {
                long currentNumber = Int64.Parse(inputLines[i]);
                slidingWindowMin = Math.Min(slidingWindowMin, currentNumber);
                slidingWindowMax = Math.Max(slidingWindowMax, currentNumber);
            }

            return (slidingWindowMin + slidingWindowMax).ToString();
        }
    }
}

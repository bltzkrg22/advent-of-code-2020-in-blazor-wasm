﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution07 : SolutionCommon
    {
        private string rulePattern = @"^(\w+ \w+) bags contain (.+)\.$";
        private string childPattern = @"(\d+) (\w+ \w+) bag"; //eg. 5 dull brown bags

        public override string PartA(string input)
        {
            var parentsOfChild = new Dictionary<string, HashSet<string>>();

            string[] rules = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            foreach (var rule in rules)
            {
                string[] parentAndChildren = Regex.Split(rule, rulePattern);

                string parent = parentAndChildren[1];

                if (parentAndChildren[2] != "no other bags")
                {
                    var children = Regex.Matches(parentAndChildren[2], childPattern);

                    // If we use anonymous var here, then match will be of type `object`!
                    foreach (Match child in children) 
                    {
                        var childColor = child.Groups[2].Value;
                        if (!parentsOfChild.ContainsKey(childColor))
                        {
                            parentsOfChild[childColor] = new HashSet<string>();
                        }
                        parentsOfChild[childColor].Add(parent);
                    }
                }
            }

            HashSet<string> answers = new HashSet<string>();

            Queue<string> possibleBags = new Queue<string>();
            possibleBags.Enqueue("shiny gold");

            while (possibleBags.Count > 0)
            {
                string currentCheck = possibleBags.Dequeue();

                if (!parentsOfChild.ContainsKey(currentCheck))
                {
                    continue;
                }

                foreach (var parent in parentsOfChild[currentCheck])
                {
                    if (answers.Add(parent) == true)
                    {
                        possibleBags.Enqueue(parent);
                    }
                }
            }

            return answers.Count.ToString();
        }

        public override string PartB(string input)
        {
            var childrenOfParent = new Dictionary<string, HashSet<(string bag, int count)>>();

            string[] rules = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            foreach (var rule in rules)
            {
                string[] parentAndChildren = Regex.Split(rule, rulePattern);

                string parent = parentAndChildren[1];
                childrenOfParent[parent] = new HashSet<(string bag, int count)>();

                if (parentAndChildren[2] != "no other bags")
                {
                    var children = Regex.Matches(parentAndChildren[2], childPattern);

                    // If we use anonymous var here, then match will be of type `object`!
                    foreach (Match child in children)
                    {
                        var childColor = child.Groups[2].Value;
                        var childCount = Int32.Parse(child.Groups[1].Value);
                        childrenOfParent[parent].Add((childColor, childCount));
                    }
                }
            }

            int answer = BagsInside("shiny gold", childrenOfParent);
            return answer.ToString();


            int BagsInside(string parentBag, Dictionary<string, HashSet<(string bag, int count)>> childrenOfParent)
            {
                int answer = 0;

                if (childrenOfParent[parentBag].Count == 0)
                {
                    return answer;
                }
                else
                {
                    foreach (var child in childrenOfParent[parentBag])
                    {
                        answer += child.count * (1 + BagsInside(child.bag, childrenOfParent));
                    }
                    return answer;
                }
            }
        }
    }
}

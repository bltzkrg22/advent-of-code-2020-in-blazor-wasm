﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution22 : SolutionCommon
    {
        private Queue<int> ParseDeck(string rawDeck)
        {
            var output = new Queue<int>();
            // [1..] is a slice operator, that skips the first element from array
            foreach (var line in rawDeck.Split("\n", StringSplitOptions.RemoveEmptyEntries)[1..])
            {
                output.Enqueue(Int32.Parse(line));
            }
            return output;
        }

        // DeckScore method actually destroys the queue! But since we calculate the score
        // only after the game of Crab Combat ended, this is not a problem
        private int DeckScore(Queue<int> winnerDeck)
        {
            int score = 0;
            int multiplier = winnerDeck.Count;

            while (winnerDeck.Count > 0)
            {
                score += multiplier * winnerDeck.Dequeue();
                multiplier--;
            }

            return score;
        }

        private Dictionary<string, bool> PreviousResultsMemory = new Dictionary<string, bool>();

        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            Queue<int> myDeck = ParseDeck(inputLines[0]);
            Queue<int> crabDeck = ParseDeck(inputLines[1]);


            while (myDeck.Count > 0 && crabDeck.Count > 0)
            {
                int myCard = myDeck.Dequeue();
                int crabCard = crabDeck.Dequeue();

                if (myCard > crabCard)
                {
                    myDeck.Enqueue(myCard);
                    myDeck.Enqueue(crabCard);
                }
                else
                {
                    crabDeck.Enqueue(crabCard);
                    crabDeck.Enqueue(myCard);
                }
            }

            Queue<int> winnerDeck;
            if (myDeck.Count == 0)
            {
                winnerDeck = crabDeck;
            }
            else
            {
                winnerDeck = myDeck;
            }

            return DeckScore(winnerDeck).ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            Queue<int> myDeck = ParseDeck(inputLines[0]);
            Queue<int> crabDeck = ParseDeck(inputLines[1]);


            bool gameResult = RecursiveCombat(myDeck, crabDeck);

            Queue<int> winnerDeck;
            if (gameResult == false)
            {
                winnerDeck = crabDeck;
            }
            else
            {
                winnerDeck = myDeck;
            }

            return DeckScore(winnerDeck).ToString();



            // Returns true if the player the winner, and false if the crab is the winner
            bool RecursiveCombat(Queue<int> myDeck, Queue<int> crabDeck)
            {
                var hash = DeckHash(myDeck, crabDeck);
                if (PreviousResultsMemory.ContainsKey(hash))
                {
                    return PreviousResultsMemory[hash];
                }

                var hashMemory = new HashSet<string>();
                bool endCondition = false;
                while (!endCondition)
                {
                    if (!hashMemory.Add(DeckHash(myDeck, crabDeck)))
                    {
                        foreach (var hashm in hashMemory)
                        {
                            PreviousResultsMemory[hashm] = true;
                        }
                        return true;
                    }

                    int myCard = myDeck.Dequeue();
                    int crabCard = crabDeck.Dequeue();

                    if (myDeck.Count >= myCard && crabDeck.Count >= crabCard)
                    {
                        var mySubDeck = new Queue<int>(myDeck.Take(myCard));
                        var crabSubDeck = new Queue<int>(crabDeck.Take(crabCard));

                        if (RecursiveCombat(mySubDeck, crabSubDeck))
                        {
                            myDeck.Enqueue(myCard);
                            myDeck.Enqueue(crabCard);
                        }
                        else
                        {
                            crabDeck.Enqueue(crabCard);
                            crabDeck.Enqueue(myCard);
                        }
                    }
                    else
                    {
                        if (myCard > crabCard)
                        {
                            myDeck.Enqueue(myCard);
                            myDeck.Enqueue(crabCard);
                        }
                        else
                        {
                            crabDeck.Enqueue(crabCard);
                            crabDeck.Enqueue(myCard);
                        }
                    }

                    if (crabDeck.Count == 0)
                    {
                        foreach (var hashm in hashMemory)
                        {
                            PreviousResultsMemory[hashm] = true;
                        }
                        return true;
                    }
                    else if (myDeck.Count == 0)
                    {
                        foreach (var hashm in hashMemory)
                        {
                            PreviousResultsMemory[hashm] = false;
                        }
                        return false;
                    }
                }

                throw new ApplicationException($"{nameof(RecursiveCombat)} should return an answer before reaching this line.");
            }


            // We will store the result of this method to check if the same exact decks were
            // already encountered previously
            string DeckHash(Queue<int> myDeck, Queue<int> crabDeck)
                => String.Join(",", myDeck.Select(element => element.ToString()).ToArray()) + ";" 
                    + String.Join(",", crabDeck.Select(element => element.ToString()).ToArray());

        }
    }
}

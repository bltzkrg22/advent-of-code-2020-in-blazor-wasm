﻿using System;
using System.Text.RegularExpressions;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution18 : SolutionCommon
    {
        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            long sum = 0L;
            foreach (var line in inputLines)
            {
                sum += Int64.Parse(AnalyzeEquation(line));
            }

            return sum.ToString();


            string AnalyzeEquation(string equation)
            {
                string equationMutable = equation;

                string regexPattern = @"(.*?)\(([^(]*?)\)(.*?)$";

                while (equationMutable.Contains('('))
                {
                    string[] processedEquation = Regex.Split(equationMutable, regexPattern);
                    string simpleResult = AnalyzeSimplest(processedEquation[2]);
                    equationMutable = processedEquation[1] + simpleResult + processedEquation[3];
                }

                return AnalyzeSimplest(equationMutable);
            }

            // This method analyzes equations which do not have any brackets
            // For PartA addition and multiplication have the same priority
            string AnalyzeSimplest(string equation)
            {
                string equationMutable = equation;

                string regexPattern = @"(\d+) (\+|\*) (\d+)(.*?)$";

                while (equationMutable.Contains('+') || equationMutable.Contains('*'))
                {
                    string[] processedEquation = Regex.Split(equationMutable, regexPattern);
                    long leftNumber = long.Parse(processedEquation[1]);
                    char currentOperator = processedEquation[2][0];
                    long rightNumber = long.Parse(processedEquation[3]);
                    long operationResult = 0;

                    switch (currentOperator)
                    {
                        case '+':
                            operationResult = leftNumber + rightNumber;
                            break;
                        case '*':
                            operationResult = leftNumber * rightNumber;
                            break;
                    }

                    equationMutable = operationResult.ToString() + processedEquation[4];
                }


                return equationMutable;
            }
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            long sum = 0L;
            foreach (var line in inputLines)
            {
                sum += Int64.Parse(AnalyzeEquation(line));
            }

            return sum.ToString();



            static string AnalyzeEquation(string equation)
            {
                string equationMutable = equation;

                string regexPattern = @"(.*?)\(([^(]*?)\)(.*?)$";

                while (equationMutable.Contains('('))
                {
                    string[] processedEquation = Regex.Split(equationMutable, regexPattern);
                    string simpleResult = AnalyzeAddition(processedEquation[2]);
                    equationMutable = processedEquation[1] + simpleResult + processedEquation[3];
                }

                return AnalyzeAddition(equationMutable);
            }


            static string AnalyzeAddition(string equation)
            {
                string equationMutable = equation;

                string regexPattern = @"^(.*?)(\d+) \+ (\d+)(.*?)$";

                while (equationMutable.Contains('+'))
                {
                    string[] processedEquation = Regex.Split(equationMutable, regexPattern);

                    long leftNumber = long.Parse(processedEquation[2]);
                    long rightNumber = long.Parse(processedEquation[3]);
                    long operationResult = leftNumber + rightNumber;

                    equationMutable = processedEquation[1] + operationResult.ToString() + processedEquation[4];
                }

                return AnalyzeMultiplication(equationMutable);
            }



            static string AnalyzeMultiplication(string equation)
            {
                string equationMutable = equation;

                string regexPattern = @"(\d+) \* (\d+)(.*?)$";

                while (equationMutable.Contains('*'))
                {
                    string[] processedEquation = Regex.Split(equationMutable, regexPattern);
                    long leftNumber = long.Parse(processedEquation[1]);
                    long rightNumber = long.Parse(processedEquation[2]);
                    long operationResult = leftNumber * rightNumber;

                    equationMutable = operationResult.ToString() + processedEquation[3];
                }


                return equationMutable;
            }
        }
    }
}

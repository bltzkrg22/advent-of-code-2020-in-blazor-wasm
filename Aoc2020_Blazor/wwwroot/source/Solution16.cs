﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution16 : SolutionCommon
    {
        // departure location: 49-258 or 268-960
        private const string rangePattern = @"(.*?): (\d+)-(\d+) or (\d+)-(\d+)";

        private class Range
        {
            public string Name { get;  }
            private int Amin { get; }
            private int Amax { get; }
            private int Zmin { get; }
            private int Zmax { get; }

            public Range(string name, int amin, int amax, int zmin, int zmax)
            {
                Name = name;
                Amin = amin;
                Amax = amax;
                Zmin = zmin;
                Zmax = zmax;
            }

            public bool IsValueInRange(int value) 
                => ((Amin <= value) && (value <= Amax)) || ((Zmin <= value) && (value <= Zmax));
        }
        private List<Range> ParseRanges(string[] ranges)
        {
            var rangeList = new List<Range>();

            foreach (var range in ranges)
            {
                string[] matchGroups = Regex.Split(range, rangePattern);

                string name = matchGroups[1];
                int amin = Int32.Parse(matchGroups[2]);
                int amax = Int32.Parse(matchGroups[3]);
                int zmin = Int32.Parse(matchGroups[4]);
                int zmax = Int32.Parse(matchGroups[5]);

                rangeList.Add(new Range(name, amin, amax, zmin, zmax));
            }

            return rangeList;
        }

        private List<int> ParseTicket(string ticket) => ticket.Split(',').Select(Int32.Parse).ToList();

        public override string PartA(string input)
        {
            // There are three input groups separated by double newlines
            string[] inputGroups = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            // More meaningful names
            string[] ranges = inputGroups[0].Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            //string myTicket = inputGroups[1].Split(":", StringSplitOptions.RemoveEmptyEntries)[1];
            string[] otherTickets = inputGroups[2].Split(":", StringSplitOptions.None)[1]
                                                  .Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);

            var rangeList = ParseRanges(ranges);

            int errorRate = 0;

            // No anonymous var, it was too easy to confuse the types
            foreach (string ticket in otherTickets)
            {
                var ticketValues = ParseTicket(ticket);
                foreach (int field in ticketValues)
                {
                    bool fieldIsInsideRange = false;
                    foreach (Range range in rangeList)
                    {
                        fieldIsInsideRange = range.IsValueInRange(field);
                        // Stop the search after the first match is found
                        if (fieldIsInsideRange)
                        {
                            break;
                        }
                    }

                    // If no match was found, this ticket is invalid
                    if (!fieldIsInsideRange)
                    {
                        errorRate += field;
                    }
                }
            }

            return errorRate.ToString();
        }

        public override string PartB(string input)
        {
            // There are three input groups separated by double newlines
            string[] inputGroups = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            // More meaningful names
            string[] ranges = inputGroups[0].Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            List<int> myTicket = ParseTicket(inputGroups[1].Split(":", StringSplitOptions.RemoveEmptyEntries)[1]);
            string[] otherTickets = inputGroups[2].Split(":", StringSplitOptions.None)[1]
                                                  .Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);

            List<Range> rangeList = ParseRanges(ranges);
            // A "ticket object" is here represented by a List of ints
            var validTickets = new List<List<int>>();

            foreach (string ticket in otherTickets)
            {
                bool ticketIsValid = true;

                var ticketValues = ParseTicket(ticket);
                foreach (int field in ticketValues)
                {
                    bool fieldIsInsideRange = false;
                    foreach (Range range in rangeList)
                    {
                        fieldIsInsideRange = range.IsValueInRange(field);
                        // Stop the search after the first match is found
                        if (fieldIsInsideRange)
                        {
                            break;
                        }
                    }

                    // If no match was found, this ticket is invalid
                    if (!fieldIsInsideRange)
                    {
                        ticketIsValid = false;
                    }
                }

                if (ticketIsValid)
                {
                    validTickets.Add(ticketValues);
                }
            }

            // At this point we have a List validTickets with all valid tickets


            // Remember that the number of fields in a single ticket ("number of columns")
            // must be the same as the number of ranges ("number of rows")
            int numberOfFields = myTicket.Count;


            // candidateColumns[i] = HashSet of all possible columns in input, that can be matched to
            // a certain rangeList[i]
            var candidateColumns = new List<HashSet<int>>();
            for (int i = 0; i < numberOfFields; i++)
            {
                candidateColumns.Add(new HashSet<int>());
            }

            for (int i = 0; i < numberOfFields; i++) // loop over List<Range> rangeList
            {
                for (int j = 0; j < numberOfFields; j++) // loop over columns
                {
                    bool errorFound = false;

                    foreach (var ticket in validTickets)
                    {
                        errorFound = !rangeList[i].IsValueInRange(ticket[j]);
                        if (errorFound)
                        {
                            break;
                        }
                    }
                    if (!errorFound)
                    {
                        candidateColumns[i].Add(j);
                    }
                }
            }


            // At this point candidateColumns[i] should already include all columns,
            // that can be matched to rangeList[i]. Now we check for which candidateColumns
            // there is only one matching range, i.e. candidateColumns[i] == 1!

            var columnToRangeMatches = new Dictionary<int, int>();


            int unmatchedColumns = numberOfFields;

            while (unmatchedColumns > 0)
            {
                for (int i = 0; i < numberOfFields; i++) // loop over candidateColumns
                {
                    if (candidateColumns[i].Count == 1)
                    {
                        int columnMatched = candidateColumns[i].First();
                        columnToRangeMatches[i] = columnMatched;
                        foreach (var column in candidateColumns)
                        {
                            column.Remove(columnMatched);
                        }
                        unmatchedColumns--;
                    }
                }
            }

            // At this point columnToRangeMatches should be fully populated
            // columnToRangeMatches[x] = y means that the range in y-th row from input
            // is matched to x-th column in all ticket data
            long productAnswer = 1L;
            for (int i = 0; i < numberOfFields; i++)
            {
                if (rangeList[i].Name.StartsWith("departure"))
                {
                    productAnswer *= myTicket[columnToRangeMatches[i]];
                }
            }

            return productAnswer.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution19 : SolutionCommon
    {
        private const string rulePattern = @"^(\d+): (""a""|""b""|.*?)$";
        private const int ruleRecurrenceDepth = 3;
        private readonly Regex digit = new Regex(@"\d");

        private void RemoveNonCircularReferences(Dictionary<int, string> ruleSet)
        {
            while (ruleSet.Count > 1)
            {
                // Inside this loop we will find a rule X that does not have any digits
                // If we find such a rule X, we will modify all parent rules that include
                // such a rule with explicit ruleText instead of ruleId reference,
                // and remove rule X from dictionary (since it is no longer referenced)
                int simpleRuleIndex = ruleSet.Where(rule => !digit.IsMatch(rule.Value))
                            .Select(rule => rule.Key).FirstOrDefault();

                if (simpleRuleIndex == 0)
                {
                    /*
                    * We break the loop if FirstOrDefault == 0. This can happen in two cases:
                    * 1) Rule 0 was made explicit, with no references, before other rules -
                    * while this is possible for general inputs, there is a silent assumpion
                    * in input data that rule 0 is always the root rule
                    * 2) There are no more rules remove, i.e. there are some circular 
                    * references. 
                    */
                    break;
                }

                var simpleRuleValue = ruleSet[simpleRuleIndex].Replace(" ", String.Empty);

                ruleSet.Remove(simpleRuleIndex);

                // Parentheses litter the result visually and make it pretty much unreadable,
                // but they make it easier to understand the creation process
                string replaceWhat = @"\b(" + simpleRuleIndex.ToString() + @")\b";
                string replaceWith = @"(" + simpleRuleValue + @")";

                foreach (var rule in ruleSet)
                {
                    ruleSet[rule.Key] = Regex.Replace(rule.Value, replaceWhat, replaceWith);
                }
            }
        }

        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            string[] rawRules = inputLines[0].Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            string[] messages = inputLines[1].Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var ruleSet = new Dictionary<int, string>();
            foreach (var rule in rawRules)
            {
                // `18: 48 48` or `48: "b"`
                string[] splitRule = Regex.Split(rule, rulePattern);
                int ruleId = Int32.Parse(splitRule[1]);
                // Strip the quotation marks if the rule is defined through a letter
                string ruleText = splitRule[2].Replace("\"", string.Empty);

                ruleSet[ruleId] = ruleText;
            }


            /*
            * This is a pretty hacky solution, but it works. From all the rules we will create
            * a *regular expression* (yes!), that we will later use to test all messages
            */

            RemoveNonCircularReferences(ruleSet);

            string finalPattern = @"^" + ruleSet[0].Replace(" ", string.Empty) + @"$";

            int correctMessageCounter = 0;
            foreach (var message in messages)
            {
                if (Regex.IsMatch(message, finalPattern))
                {
                    correctMessageCounter++;
                }
            }

            return correctMessageCounter.ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            string[] rawRules = inputLines[0].Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            string[] messages = inputLines[1].Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var ruleSet = new Dictionary<int, string>();
            foreach (var rule in rawRules)
            {
                // `18: 48 48` or `48: "b"`
                string[] splitRule = Regex.Split(rule, rulePattern);
                int ruleId = Int32.Parse(splitRule[1]);

                string ruleText = splitRule[2].Replace("\"", string.Empty);

                ruleSet[ruleId] = ruleText;
            }

            // Overwrite input data with the special rules for PartB
            ruleSet[8] = "42 | 42 8";
            ruleSet[11] = "42 31 | 42 11 31";

            RemoveNonCircularReferences(ruleSet);

            // At this point we need to handle the special rules for partB. We will do so by  
            int[] magicNumbers = { 8, 11 };

            // The following code will for example replace rule 8: `42 | 42 8` with:
            // `42`, then with `42 | 42 42`, then `42 | 42 42 | 42 42 42` etc.
            // We will do so for the number of times specified in constant ruleRecurrenceDepth.
            foreach (var number in magicNumbers)
            {
                string regexPattern = @"^(.*?) \|(.*?) (" + number.ToString() + @")(.*?)$";
                string[] baseRule = Regex.Split(ruleSet[number], regexPattern);

                // baseRule[1] = current replacement
                // baseRule[2] = before recursion
                // baseRule[4] = after recursion

                string currentReplacement = baseRule[1];
                string newRule = baseRule[1];
                for (int i = 0; i <= ruleRecurrenceDepth; i++)
                {
                    newRule += "|" + baseRule[2] + currentReplacement + baseRule[4];
                    currentReplacement = baseRule[2] + currentReplacement + baseRule[4];
                }

                ruleSet[number] = newRule.Replace(" ", String.Empty); ;
            }





            RemoveNonCircularReferences(ruleSet);


            string finalPattern = @"^" + ruleSet[0].Replace(" ", string.Empty) + @"$";

            int correctMessageCounter = 0;
            foreach (var message in messages)
            {
                if (Regex.IsMatch(message, finalPattern))
                {
                    correctMessageCounter++;
                }
            }

            return correctMessageCounter.ToString();
        }
    }
}

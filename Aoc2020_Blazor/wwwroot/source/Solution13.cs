﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution13 : SolutionCommon
    {
        // Code borrowed from Rosetta Code:
        // https://rosettacode.org/wiki/Chinese_remainder_theorem
        // and adapted to support Int64 type answers
        private static class ChineseRemainderTheorem
        {
            public static long Solve(int[] n, int[] a)
            {
                long prod = 1L;

                foreach (var number in n)
                {
                    prod *= number;
                }

                long p;
                long sm = 0;
                for (int i = 0; i < n.Length; i++)
                {
                    p = prod / n[i];
                    sm += a[i] * p * ModularMultiplicativeInverse(p, n[i]);
                }
                return sm % prod;
            }

            private static long ModularMultiplicativeInverse(long a, int mod)
            {
                long b = a % mod;
                for (int x = 1; x < mod; x++)
                {
                    if ((b * x) % mod == 1)
                    {
                        return x;
                    }
                }
                return 1;
            }
        }


        public override string PartA(string input)
        {
            var inputReader = new StringReader(input);

            int timestamp = Int32.Parse(inputReader.ReadLine());
            var buslist = new List<int>();

            string rawBuslist = inputReader.ReadLine();
            inputReader.Close();

            string pattern = @"\d+";
            var matches = Regex.Matches(rawBuslist, pattern);
            foreach (Match match in matches)
            {
                int busId = Int32.Parse(match.Groups[0].Value);
                buslist.Add(busId);
            }

            int earliestTime = Int32.MaxValue;
            int earliestBusId = -1;
            foreach (var busId in buslist)
            {
                int nextDep = NextDeparture(timestamp, busId);
                if (nextDep < earliestTime)
                {
                    earliestTime = nextDep;
                    earliestBusId = busId;
                }
            }

            int waitTime = earliestTime - timestamp;

            return (earliestBusId * waitTime).ToString();



            // Local helper function
            int NextDeparture(int timestamp, int id) 
                => timestamp % id == 0 ? timestamp : timestamp - timestamp % id + id;
        }

        public override string PartB(string input)
        {
            // This time we can't just discard 'x'-es  from the input, so we won't
            // use regular expressions to parse the input

            var buslist = new List<(int busId, int offset)>();

            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            string[] rawEntries = inputLines[1].Split(",", StringSplitOptions.TrimEntries);

            for (int i = 0; i < rawEntries.Length; i++)
            {
                if (rawEntries[i][0] != 'x')
                {
                    buslist.Add((Int32.Parse(rawEntries[i]), i));
                }
            }

            int busCount = buslist.Count;
            int[] mods = new int[busCount];
            int[] remainders = new int[busCount];

            for (int i = 0; i < busCount; i++)
            {
                mods[i] = buslist[i].busId;
                remainders[i] = ((-buslist[i].offset % buslist[i].busId) + buslist[i].busId) % buslist[i].busId;
            }

            /*
            * Here we make a silent assumption, that all bus IDs are prime numbers!
            * This is never explicitly stated in problem description, but the problem
            * heavily nudges us towards Chinese Remaider Theorem, which requires
            * that the divisors (here: bus IDs, stored in `mods` array) are pairwise coprime.
            * https://en.wikipedia.org/wiki/Chinese_remainder_theorem
            */
            long answer = ChineseRemainderTheorem.Solve(mods, remainders);

            return answer.ToString();
        }
    }
}

﻿using System.Threading.Tasks;

namespace Aoc2020_Blazor.Solutions
{
    public abstract class SolutionCommon : ISolution
    {
        public abstract string PartA(string input);

        public abstract string PartB(string input);

        public virtual Task<string> PartAAsync(string input) => Task.FromResult(PartA(input));

        public virtual Task<string> PartBAsync(string input) => Task.FromResult(PartB(input));
    }
}

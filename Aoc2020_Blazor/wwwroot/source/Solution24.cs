﻿using System;
using System.Collections.Generic;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution24 : SolutionCommon
    {
        private const int SimulationTime = 100;

        private class TilePosition
        {
            public int Alpha { get; init; }
            public int Beta { get; init; }
            public int Gamma { get; init; }

            public (int, int) EquivClass { get; private set; }

            public TilePosition(int a, int b, int c)
            {
                Alpha = a;
                Beta = b;
                Gamma = c;

                EquivClass = CalcEquivClass();
            }

            public TilePosition(int a, int b)
            {
                Alpha = a;
                Beta = b;
                Gamma = 0;

                EquivClass = CalcEquivClass();
            }

            private (int, int) CalcEquivClass() => ( Alpha - Gamma, Beta - Gamma);
        }

        List<(int, int)> Directions = new List<(int, int)>()
            { (1, 0), (1, 1), (0, 1), (-1, 0), (-1, -1), (0, -1) };

        private TilePosition ParseDirections(string directions)
        {
            int charIndex = 0;
            int alpha = 0;
            int beta = 0;
            int gamma = 0;

            while (charIndex < directions.Length)
            {
                char currentChar = directions[charIndex];
                if (currentChar == 'e')
                {
                    alpha += 1;
                }
                else if (currentChar == 'w')
                {
                    alpha -= 1;
                }
                else if (currentChar == 'n')
                {
                    charIndex++;
                    currentChar = directions[charIndex];
                    if (currentChar == 'e')
                    {
                        gamma -= 1;
                    }
                    else if (currentChar == 'w')
                    {
                        beta += 1;
                    }
                    else
                    {
                        throw new ApplicationException("Invalid directions!");
                    }
                }
                else if (currentChar == 's')
                {
                    charIndex++;
                    currentChar = directions[charIndex];
                    if (currentChar == 'e')
                    {
                        beta -= 1;
                    }
                    else if (currentChar == 'w')
                    {
                        gamma += 1;
                    }
                    else
                    {
                        throw new ApplicationException("Invalid directions!");
                    }
                }
                else
                {
                    throw new ApplicationException("Invalid directions!");
                }
                charIndex++;
            }

            return new TilePosition(alpha, beta, gamma);
        }



        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var tilesToFlip = new HashSet<(int, int)>();

            foreach (var line in inputLines)
            {
                var tile = ParseDirections(line);
                if (!tilesToFlip.Add(tile.EquivClass))
                {
                    tilesToFlip.Remove(tile.EquivClass);
                }
            }

            return tilesToFlip.Count.ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var activeTiles = new HashSet<(int p, int q)>();

            foreach (var line in inputLines)
            {
                var tile = ParseDirections(line);
                if (!activeTiles.Add(tile.EquivClass))
                {
                    activeTiles.Remove(tile.EquivClass);
                }
            }

            for (int i = 1; i <= SimulationTime; i++)
            {
                var nextActiveTiles = new HashSet<(int p, int q)>();
                var inactiveTiles = new Dictionary<(int p, int q), int>();

                foreach (var tile in activeTiles)
                {
                    var activeNeighbours = 0;
                    foreach (var neighbour in GetNeighbours(tile))
                    {
                        if (activeTiles.Contains(neighbour))
                        {
                            activeNeighbours++;
                        }
                        else
                        {
                            inactiveTiles[neighbour] = inactiveTiles.GetValueOrDefault(neighbour) + 1;
                        }
                    }

                    if (activeNeighbours == 1 || activeNeighbours == 2)
                    {
                        nextActiveTiles.Add(tile);
                    }
                }

                foreach (var (tile, activeNeighbours) in inactiveTiles)
                {
                    if (activeNeighbours == 2)
                    {
                        nextActiveTiles.Add(tile);
                    }
                }
                activeTiles = nextActiveTiles;
            }

            return activeTiles.Count.ToString();



            List<(int p, int q)> GetNeighbours((int p, int q) tile)
            {
                var answer = new List<(int p, int q)>();

                foreach (var (dp, dq) in Directions)
                {
                    answer.Add((dp + tile.p, dq + tile.q));
                }

                return answer;
            }
        }
    }
}

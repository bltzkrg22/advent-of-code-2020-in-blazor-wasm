﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution17 : SolutionCommon
    {
        private const int SimulationTime = 6;
        private const char ActiveCube = '#';

        private readonly List<(int x, int y, int z)> Directions3D =
            (new[] { -1, 0, 1 }).SelectMany(xCoord =>
                (new[] { -1, 0, 1 }).SelectMany(yCoord =>
                    (new[] { -1, 0, 1 }).Select(zCoord => (xCoord, yCoord, zCoord))))
                        .Where(coords => coords != (0, 0, 0)).ToList();


        private readonly List<(int x, int y, int z, int w)> Directions4D = 
            (new[] { -1, 0, 1}).SelectMany(x =>
                (new[] { -1, 0, 1 }).SelectMany(y =>
                    (new[] { -1, 0, 1 }).SelectMany(z =>
                        (new[] { -1, 0, 1 }).Select(w => (x, y, z, w)))))
                            .Where(coords => coords != (0, 0, 0, 0)).ToList();

        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var activatedCubes = new HashSet<(int x, int y, int z)>();

            for (int y = 0; y < inputLines.Length; y++)
            {
                for (int x = 0; x < inputLines[0].Length; x++)
                {
                    if (inputLines[y][x] == ActiveCube)
                    {
                        activatedCubes.Add((x, y, 0));
                    }
                }
            }
            // At this point we finished scanning the input

            for (int turn = 1; turn <= SimulationTime; turn++)
            {
                var nextActivatedCubes = new HashSet<(int x, int y, int z)>();
                // Dictionary value is the number of active neighbours, which we count
                // for each inactive cube (but from pov of those active neighbour cubes!)
                var inactiveCubes = new Dictionary<(int x, int y, int z), int>();

                foreach (var cube in activatedCubes)
                {
                    var activeNeighbours = 0;
                    foreach (var neighbour in GetNeighbours(cube))
                    {
                        if (activatedCubes.Contains(neighbour))
                        {
                            activeNeighbours++;
                        }
                        else
                        {
                            inactiveCubes[neighbour] = inactiveCubes.GetValueOrDefault(neighbour) + 1;
                        }
                    }

                    if (activeNeighbours == 2 || activeNeighbours == 3)
                    {
                        nextActivatedCubes.Add(cube);
                    }
                }

                foreach (var (cube, activeNeighbours) in inactiveCubes)
                {
                    if (activeNeighbours == 3)
                    {
                        nextActivatedCubes.Add(cube);
                    }
                }
                activatedCubes = nextActivatedCubes;
            }

            return activatedCubes.Count.ToString();


            List<(int x, int y, int z)> GetNeighbours((int x, int y, int z) cube)
            {
                var answer = new List<(int x, int y, int z)>();

                foreach (var (dx, dy, dz) in Directions3D)
                {
                    answer.Add((dx + cube.x, dy + cube.y, dz + cube.z));
                }

                return answer;
            }
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var activatedCubes = new HashSet<(int x, int y, int z, int w)>();
            
            for (int y = 0; y < inputLines.Length; y++)
            {
                for (int x = 0; x < inputLines[0].Length; x++)
                {
                    if (inputLines[y][x] == ActiveCube)
                    {
                        activatedCubes.Add((x, y, 0, 0));
                    }
                }
            }

            // At this point we finished scanning the input

            for (int turn = 1; turn <= SimulationTime; turn++)
            {
                var nextActivatedCubes = new HashSet<(int x, int y, int z, int w)>();
                // Dictionary value is the number of active neighbours, which we count
                // for each inactive cube (but from pov of those active neighbour cubes!)
                var inactiveCubes = new Dictionary<(int x, int y, int z, int w), int>();

                foreach (var cube in activatedCubes)
                {
                    var activeNeighbours = 0;
                    foreach (var neighbour in GetNeighbours(cube))
                    {
                        if (activatedCubes.Contains(neighbour))
                        {
                            activeNeighbours++;
                        }
                        else
                        {
                            inactiveCubes[neighbour] = inactiveCubes.GetValueOrDefault(neighbour) + 1;
                        }
                    }

                    if (activeNeighbours == 2 || activeNeighbours == 3)
                    {
                        nextActivatedCubes.Add(cube);
                    }
                }

                foreach (var (cube, activeNeighbours) in inactiveCubes)
                {
                    if (activeNeighbours == 3)
                    {
                        nextActivatedCubes.Add(cube);
                    }
                }
                activatedCubes = nextActivatedCubes;
            }

            return activatedCubes.Count.ToString();


            List<(int x, int y, int z, int w)> GetNeighbours((int x, int y, int z, int w) cube)
            {
                var answer = new List<(int x, int y, int z, int w)>();

                foreach (var (dx, dy, dz, dw) in Directions4D)
                {
                    answer.Add((dx + cube.x, dy + cube.y, dz + cube.z, dw + cube.w));
                }

                return answer;
            }
        }
    }
}

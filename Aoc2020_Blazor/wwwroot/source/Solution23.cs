﻿using System;
using System.Linq;
using System.Text;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution23 : SolutionCommon
    {
        //private const int cupNumberA = 9;
        private const int turnNumberA = 100;
        private const int cupNumberB = 1_000_000; // one milion
        private const int turnNumberB = 10_000_000; // ten million
        private string PartCommon(string input, int cupNumber, int turnNumber, char part)
        {
            int[] inputArray = input.Select(ch => ch - '0').ToArray();
            int maxInputLabel = inputArray.Max();

            // The label of the last cup = maxInputLabel + (number of added cups); additionally,
            // (number of added cups) = cupNumber - inputArray.Length
            // Finally, we need to add 1, since the array is indexed from 0
            int maxCupLabel = maxInputLabel + cupNumber - inputArray.Length;
            int[] nextCupLabel = new int[maxCupLabel + 1];


            // There is no cup with label 0, this value will never be used
            // Every cup label that is not present should also be initialized to 0 and never used
            nextCupLabel[0] = 0;


            // Read the input data
            int currentCupNumber = inputArray[0];
            for (int i = 1; i < inputArray.Length; i++)
            {
                nextCupLabel[currentCupNumber] = inputArray[i];
                currentCupNumber = nextCupLabel[currentCupNumber];
            }


            // If cupNumber == inputArray.Length, loop the references
            if (cupNumber == inputArray.Length)
            {
                nextCupLabel[currentCupNumber] = inputArray[0];
            }



            // If cupNumber > inputArray.Length, generate (cupNumber - inputArray.Length) new cup labels,
            // starting from maxInputLabel + 1, and finaly loop the references
            if (cupNumber > inputArray.Length)
            {
                foreach (int cuplabel in Enumerable.Range(maxInputLabel + 1, cupNumber - inputArray.Length).ToArray())
                {
                    nextCupLabel[currentCupNumber] = cuplabel;
                    currentCupNumber = nextCupLabel[currentCupNumber];
                }

                nextCupLabel[currentCupNumber] = inputArray[0];
            }

            currentCupNumber = inputArray[0];
            for (int i = 1; i <= turnNumber; i++)
            {
                currentCupNumber = CupShuffle(currentCupNumber, nextCupLabel, maxCupLabel);
            }

            if (part == 'B')
            {
                long firstClockwise = nextCupLabel[1];
                long secondClockwise = nextCupLabel[firstClockwise];
                return (firstClockwise * secondClockwise).ToString();
            }
            else
            {
                return PrintCupsAfterOne(nextCupLabel);
            }




            int CupShuffle(int currentCup, int[] cupCircle, int maxCupLabel)
            {
                // maxCupLabel passed only for convenience
                int[] threeClockwiseCupLabels = new int[3];

                // Determine the labels of the next three cups
                threeClockwiseCupLabels[0] = cupCircle[currentCup];
                threeClockwiseCupLabels[1] = cupCircle[threeClockwiseCupLabels[0]];
                threeClockwiseCupLabels[2] = cupCircle[threeClockwiseCupLabels[1]];

                // Take away those labels from circle, by linking the reference from currentCup to the fourth cup in circle
                cupCircle[currentCup] = cupCircle[threeClockwiseCupLabels[2]];


                // Determine destination cup label
                int destinationLabel = DecreaseWithWraparound(currentCup, maxCupLabel);
                while (threeClockwiseCupLabels.Contains(destinationLabel))
                {
                    destinationLabel = DecreaseWithWraparound(destinationLabel, maxCupLabel);
                }

                // Insert three cups back. First memorize the cup after insertion point!
                int afterDestinationLabel = cupCircle[destinationLabel];
                cupCircle[destinationLabel] = threeClockwiseCupLabels[0];
                cupCircle[threeClockwiseCupLabels[2]] = afterDestinationLabel;

                // Return "the next currentCup"
                return cupCircle[currentCup];
            }

            int DecreaseWithWraparound(int number, int maxNumber)
            {
                number--;
                return number <= 0 ? (maxNumber + number) : number;
            }

            string PrintCupsAfterOne(int[] cupCircle)
            {
                var answer = new StringBuilder();

                // We exclude cup 1 from printing, and keep in mind that index 0 is "wasted"
                // Therefore i <= (cupCircle.Length - 2)
                int indexToPrint = 1;
                for (int i = 1; i <= cupCircle.Length - 2; i++)
                {
                    answer.Append(cupCircle[indexToPrint]);
                    indexToPrint = cupCircle[indexToPrint];
                }

                return answer.ToString();
            }
        }



        public override string PartA(string input) => PartCommon(input, input.Length, turnNumberA, 'A');

        public override string PartB(string input) => PartCommon(input, cupNumberB, turnNumberB, 'B');

    }
}

﻿## My solution

Another problem that calls for dynamic programming approach.

**Part A** is trivial – we just need to sort the list of adapters and count all differences between two consecutive adapters (including the difference between the first adapter and the outlet, and also between the device and the final adapter).

-----

To solve **Part B**, we must make a following observation:

The number of different adapter chains that will have joltage n at the end is equal to the sum of

- number of chains that will have joltage n - 3 at the end,
- number of chains that will have joltage n - 2 at the end,
- and number of chains that will have joltage n - 1 at the end.

Note that if, for example, the adapter with joltage n - 2 does not exist, then there are *zero* chains that have such joltage at the end!

Consider adapters `1, 2, 3, 4`. There is obviously only one chain that ends with 1. There are two chains that end with 2: `0 → 1 → 2` and `0 → 2`. There are *four* chains that end with 3:

```
0 → 1 → 3
0 → 1 → 2 → 3      0 → 2 → 3
0 → 3
```

one chain that ends with adapter 1, two chains that end with adapter 2, and we must include the plugging the adapter directly into the outlet.

Finally, there are *seven* chains that end with 4 – there is one chain that ends with adapter 1, two that end with adapter 2, and four that end with adapter 3.

```
0 → 1 → 4
0 → 1 → 2          0 → 2 → 4
0 → 1 → 2 → 3      0 → 1 → 3 → 4      0 → 2 → 3      0 → 3 → 4 
```

-----

We will create three dynamic variables – tuples `(int joltage, long paths)`, that will hold the joltages of previous three adapters. For each subsequent adapter, we will sum the number of paths of those adapters, for which the joltage difference is at most 3. (Note that “three previous adapters” does not necessarily mean “adapters with joltages n - 3, n - 2 and n - 1”!)

Initializng the dynamic variables to `(0, 0)`, `(0, 0)` and `(0, 1)` is a trick that allows us to start the for loop from the first adapter, instead of manually calculating the number of paths for the first three adapters.

Since the difference between the final adapter and the device is always equal to 3, the chain must include the final adapter; the answer is equal to the number of paths that include the final adapter.

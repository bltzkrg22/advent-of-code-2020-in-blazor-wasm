## My solution

[Conway’s Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) was a recurring topic during *AoC 2020*, first encountered on day 11.

There is nothing fancy or smart in my solution. We save the input data to a 2D char matrix, so that modifying singular characters is more convenient.

The rest is just a straight and naive implementation of the problem requirements. We iterate over whole 2D matrix with a double for loop, count occupied neighbours of every seat in a third for loop (hidden from sight by `CountOccupiedNeighbours` method), change the occupation status for the next cycle if conditions are met, and remember if at least one flip occurred.

It’s not very fast (it’s barely acceptable), but it works.

A possible optimization would be precomputing the seat positions (as opposed to empty spaces `.`), and then precomputing the list of neighbours for each seat. If the input matrix was sparse, i.e. it inlcuded a high percentage of empty spaces, precomputing would make a dramatic difference. *My* input was dense, though, so I decided to skip it.

-----

<div class="alert alert-info" role="alert">
<p>Consider checking out solutions from <a href="puzzles/day/17">day 17</a>, or from <strong>Part B</strong>, <a href="puzzles/day/24">day 24</a>, to see a smarter approach to the Game of Life.</p>
  
﻿## My solution

I really like regular expressions, so when a problem calls for parsing and validating data, I will usually try them first.

The passports in the input are visually pretty chaotic:
```
ecl:#eef340 eyr:2023 hcl:#c0946f pid:244684338 iyr:2020 cid:57 byr:1969 hgt:152cm

pid:303807545 cid:213 ecl:gry hcl:#fffffd
eyr:2038 byr:1951
hgt:171cm iyr:2011

hcl:#c0946f byr:1933 eyr:2025 pid:517067213 hgt:173cm
ecl:hzl
iyr:2018
```

We separate input on every double newline to get a list of all singular passports. Every data field in the passport is of pattern: `(field name):(field value)`; additionally, the semicolon character is *only* encountered as a name/value separator. (Even in invalid passports! If the semicolon could randomly appear in an invalid passport, we would obviously need to make some smarter parser). This makes it simple enough to create a regular expression pattern: `(\b\S+):(\S+\b)`. The special anchor `\b` makes sure that we handle the edges of each field correctly. Consider fragment of a password: `ecl:#eef340 eyr:2023 hcl:#c0946f`

1. `eyr:2023` – beginning and end are marked by space characters.
2. `ecl:#eef340` – beginning is marked by the beginning of the line, and end is marked by a space.

Anchor `\b` is exatly what we require, *an edge of a word*.

So, every match of pattern `(\b\S+):(\S+\b)` is some field. We save those to a `Dictionary<string, string>` data structure. We could technically create a new class as as our passport model, but for this particular problem a simple dictionary is just enough.

-----

For **Part A**, a passport is valid if it includes all seven mandatory fields. Simple enough, we check if all those fields are present in the dictionary.

For **Part B**, we must also validate that the data is correct. So we transform each requirement into an equivalent regular expression:

> - `byr` (Birth Year) - four digits; at least 1920 and at most 2002.
> - `iyr` (Issue Year) - four digits; at least 2010 and at most 2020.
> - `eyr` (Expiration Year) - four digits; at least 2020 and at most 2030.
> - `hgt` (Height) - a number followed by either cm or in:
> If cm, the number must be at least 150 and at most 193.
> If in, the number must be at least 59 and at most 76.
> - `hcl` (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
> - `ecl` (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
> - `pid` (Passport ID) - a nine-digit number, including leading zeroes.
> - `cid` (Country ID) - ignored, missing or not.

becomes:

```
private Dictionary<string, string> validationPatterns = new Dictionary<string, string> {
    {"byr", @"^(19[2-9][0-9]|20[0][0-2])$"},
    {"ecl", @"^(amb|blu|brn|gry|grn|hzl|oth)$"},
    {"eyr", @"^(202[0-9]|2030)$" },
    {"hcl", @"^(#[0-9a-f]{6})$"},
    {"hgt", @"^(1[5-8][0-9]cm|19[0-3]cm|59in|6[0-9]in|7[0-9]in)$"},
    {"iyr", @"^(201[0-9]|2020)$"},
    {"pid", @"^([0-9]{9})$"}
};
```

And we check is the value stored in the passport is a match for the corresponding regular expression.

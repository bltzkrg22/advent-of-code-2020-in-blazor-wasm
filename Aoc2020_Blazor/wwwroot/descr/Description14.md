﻿## My solution

As usual, **Part A** is straightforward. We read input line by line, if current line specifies a mask we memorize it; else we apply the mask to the memory address and map the value to the masked address. Since the all memory addresses are “initialized” to 0 by the problem description, we will use a `Dictionary<long, long>` to only record the memory addresses that are being modified. 

-----

To overwrite a single bit with either zero or one, as per problem description, we will use the following bitwise operations:

- expression `number | mask` will change all bits specified by `mask` to ones; consider example:


<pre><code>1<span class="font-weight-bold text-primary bg-light">01</span>010 | 011000 = 1<span class="font-weight-bold text-primary bg-light">11</span>010</pre></code>


- less obviously, `number & (~mask)` will change all bits specified by `mask` to zeros; consider example:

<pre><code>1<span class="font-weight-bold text-primary bg-light">01</span>010 & (~011000) = 101010 & 100111 = 1<span class="font-weight-bold text-primary bg-light">00</span>010</pre></code>

-----

In **Part B**, the *floating bits* introduce some trickyness, though not that much.  First, we apply the ones and zeroes from the mask as per problem description, and store the result as the only entry in a `List<long>`.

Then, we scan the mask for any `X`-es. With every `X` found, we take all values stored currently in the list, flip the bit marked by current `X`, and add those to the list. That operation obviously doubles the count of elements in the list with every `X` in the mask. In the end, we obtain a list of all memory addresses to overwrite.

Flipping the bits is obviously done with `number ^ mask` operation

<pre><code>1<span class="font-weight-bold text-primary bg-light">01</span>010 ^ 011000 = 1<span class="font-weight-bold text-primary bg-light">10</span>010</pre></code>

﻿## My solution

It felt pretty natural to create a new `Ship` class, that would have a (x,y) position in 2D space and a bearing, and which would have a method to update its position based on directions from the input list. The class is very barebones, and only implements what is specifically required to solve the problem.

(Bearing is the standard polar angle: 0 is east, 90 is north, 180 is west, 270 is south).

-----

The following line works around the fact that the remainder operator `%` in C# [returns a negative number if leftside number is negative](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/arithmetic-operators#remainder-operator-) – for simplicity we want the bearing to always be in range from 0 to 270.

```
Bearing = (Bearing + 360) % 360;
```

The above calculation will modify bearing from -90 to 270, from -180 to 180, from -270 to 90 – and will leave any positive bearing unchanged.

-----

In **Part B**, we store the *relative* position of waypoint from the ship, or in other words, the position of waypoint in ship’s local coordinate system. Therefore the rotation of waypoint around the ship can be calculated simply by multiplying the waypoint offset vector by [rotation matrix](https://en.wikipedia.org/wiki/Rotation_matrix#In_two_dimensions).

Then, since there are only 4 possible rotation angles, we can just precompute the possible rotation outcomes and use the following switch statement:

```
return rotAngleWrapped switch
{
    0 => (waypointX, waypointY),
    90 => (-waypointY, waypointX),
    180 => (-waypointX, -waypointY),
    270 => (waypointY, -waypointX),
    _ => throw new ApplicationException("Invalid rotation angle!")
};
```

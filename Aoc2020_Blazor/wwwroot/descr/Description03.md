﻿## My solution

A very simple method `CheckForTreeAtPosition(int x, int y, string[] inputLines)` returns `true` if the input data has a tree (character `#`) at the (x,y) position. Indices work like in a standard zero-indexed 2D matrix: (0,0) is first line, first character; (1,0) is first line, second character; (0,5) is sixth line, first character.

This piece of code:
```
if (currentColumn > (colCount - 1))
{
    currentColumn %= colCount;
}
```
is responsible for “glueing” the left and right edge of input data. Here, `colCount` is the number of columns in the input, and `currentColumn` is the current y position of the toboggan. For every `colCount` number of characters the data repeats; so if there are 11 columns in the input, the character in 12th column will be the same as in the 1st column, etc.

It would probably be more elegant to not change the position of the toboggan, but to implement the wrapping logic in `CheckForTreeAtPosition`, but since we are not interested in the final position, both approaches get the same result.

-----

Also note that there is no check for a tree in the starting point, since the description states:

>You start on the open square (`.`) in the top-left corner […]

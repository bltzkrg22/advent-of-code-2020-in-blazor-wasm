﻿## My solution

In first pass over the input list, we memorize all ingredients (gibberish words) and all allergens.

Per problem description:

> Each ingredient contains zero or one allergen.

and:

> when [allergens are] listed (as in (contains nuts, shellfish) after an ingredients list), the ingredient that contains each listed allergen will be somewhere in the corresponding ingredients list.

From that we can conclude, that ingerdient X cannot contain allergen Y, some product description lists allergen Y, but does not list ingredient X.

So in method `PossibleIngredientToAllegrenMatches`, we will create a data structure: `Dictionary<string, HashSet<string>> possibleAllergenMatches`, which initially to each ingredient matches all allergens, and then *remove* the allergens that do not pass the test described in previous paragraph.

The inert ingredients will be those, for which the value stored in `possibleAllergenMatches` is an empty hashset.

-----

In **Part B** , we need to match ingredients and allergens. This puzzle is quite similar to ticket validation from day 16.

After we remove all inert ingredients from `possibleAllergenMatches`, there will always be some ingredient that can be matched to only one allergen. We memorize that match, and remove the allergen from `possibleAllergenMatches` for other ingredients.

One thing to note is that the expected solution for **Part B** is to:

> Arrange the ingredients alphabetically by their allergen and separate them by commas to produce your canonical dangerous ingredient list.

Therefore we will store the pairs in a `SortedDictionary<string, string>`, where the key is the allergen name, and value is the matching ingredient name. This will make producting the answer as simple as outputing all values from the dictionary, since the sorting will be done for us automatically.

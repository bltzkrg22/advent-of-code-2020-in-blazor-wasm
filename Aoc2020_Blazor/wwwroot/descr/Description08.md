﻿## My solution

**Part A** is very simple. We just need to perform the operations from problem description, memorize the line numbers that were already executed (preferably in a hashset), and stop the first time a line is repated. Straighforward enough.

-----

In **Part B**, we are asked to find a single `nop` or `jmp` instruction, that after changing to `jmp` or `nop` respectively makes the program try to execute an instruction outside of the input list.

There is definitely a smarter way to do it, but the simplistic brute-force is performant enough. We just flip one instruction at a time and check if the program completes or enters an infinite loop.

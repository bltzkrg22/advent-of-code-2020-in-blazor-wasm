﻿## My solution

A very simple introductory probem. There is nothing special or hard to understand.

The most efficient solution for **Part A** should probaby involve creating a hashset of complementary numbers (a complementary number for x would be (2020 - x)); for **Part B** consult [this wikipedia article](https://en.wikipedia.org/wiki/3SUM) to find an efficient algorithm for finding three numbers in a list that sum to a given number.

That said, the input list is short enough that a simple nested for loop gets the result quickly.

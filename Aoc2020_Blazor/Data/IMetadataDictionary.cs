﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Aoc2020_Blazor.Data
{
    interface IMetadataDictionary
    {
        public Dictionary<int, ProblemMetadata> Metadata { get; }

        public Task Initialize();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Json;
using Newtonsoft.Json;

namespace Aoc2020_Blazor.Data
{
    public class ProblemMetadata
    {
        public int Day;
        public string Title;
    }

    public class MetadataDictionary : IMetadataDictionary
    {
        private string metadataText;
        private Dictionary<int, ProblemMetadata> _metadata;
        private HttpClient _http;

        private bool _initialized = false;


        public Dictionary<int, ProblemMetadata> Metadata => _metadata;

        public MetadataDictionary(HttpClient httpclient)
        {
            _http = httpclient;
        }

        public async Task Initialize()
        {
            if (_initialized == true)
            {
                // Do nothing.
            }
            else
            {
                _initialized = true;
                metadataText = await _http.GetStringAsync("metadata/metadata.json");
                _metadata = JsonConvert.DeserializeObject<Dictionary<int, ProblemMetadata>>(metadataText);
            }
        }

        //public MetadataDictionary()
        //{
        //    // metadataText = System.IO.File.ReadAllText("metadata/metadata.json");
        //    var Http = new HttpClient();
        //    metadataText =  Http.GetFromJsonAsync<Dictionary<int, ProblemMetadata>>("metadata/metadata.json");

        //    _metadata = JsonConvert.DeserializeObject<Dictionary<int, ProblemMetadata>>(metadataText);
        //}
    }


}

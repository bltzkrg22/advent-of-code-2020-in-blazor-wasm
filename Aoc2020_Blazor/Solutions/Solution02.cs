﻿using System;
using System.Text.RegularExpressions;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution02 : SolutionCommon
    {
        public override string PartA(string input)
        {
            int validPasswordCounter = 0;

            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            foreach (var currentLine in inputLines)
            {
                // 5-11 t: glhbttzvzttkdx
                string pattern = @"^(\d+)-(\d+) (\w): (.*?)$";

                var match = Regex.Match(currentLine, pattern);

                int minCount = int.Parse(match.Groups[1].Value);
                int maxCount = int.Parse(match.Groups[2].Value);
                char policyCharacter = match.Groups[3].Value[0];
                string policyPassword = match.Groups[4].Value;

                int policyCharacterCounter = 0;
                foreach (char c in policyPassword)
                {
                    if (c == policyCharacter)
                    { 
                        policyCharacterCounter++; 
                    }
                }

                if (policyCharacterCounter >= minCount && policyCharacterCounter <= maxCount)
                {
                    validPasswordCounter++;
                }
            }

            return validPasswordCounter.ToString();
        }

        public override string PartB(string input)
        {
            int validPasswordCounter = 0;

            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            foreach (var currentLine in inputLines)
            {
                // 5-11 t: glhbttzvzttkdx
                string pattern = @"^(\d+)-(\d+) (\w): (.*?)$";

                var match = Regex.Match(currentLine, pattern);

                int firstIndex = int.Parse(match.Groups[1].Value);
                int secondIndex = int.Parse(match.Groups[2].Value);
                char policyCharacter = match.Groups[3].Value[0];
                string policyPassword = match.Groups[4].Value;

                bool passwordIsValid = false;

                if (policyPassword[firstIndex - 1] == policyCharacter)
                {
                    passwordIsValid = !passwordIsValid;
                }

                if (policyPassword[secondIndex - 1] == policyCharacter)
                {
                    passwordIsValid = !passwordIsValid;
                }

                if (passwordIsValid == true)
                {
                    validPasswordCounter++;
                }

            }

            return validPasswordCounter.ToString();
        }
    }
}

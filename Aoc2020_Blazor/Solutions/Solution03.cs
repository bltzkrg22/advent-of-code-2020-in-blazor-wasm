﻿using System;
using System.Collections.Generic;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution03 : SolutionCommon
    {
        private const char Tree = '#';
        private readonly List<(int right, int down)> JumpOffsetsA = new List<(int right, int down)>()
            { (3, 1) };
        private readonly List<(int right, int down)> JumpOffsetsB = new List<(int right, int down)>()
            { (1, 1), (3, 1), (5, 1), (7, 1), (1, 2) };

        private bool CheckForTreeAtPosition(int x, int y, string[] inputLines)
            => inputLines[y][x] == Tree;

        public override string PartA(string input)
        {
            int treeCount = 0;

            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            int rowCount = inputLines.Length;
            int colCount = inputLines[0].Length;

            foreach (var offsets in JumpOffsetsA)
            {
                int currentColumn = 0;
                for (int currentRow = offsets.down; currentRow < rowCount; currentRow += offsets.down)
                {
                    currentColumn += offsets.right;

                    if (currentColumn > (colCount - 1))
                    {
                        currentColumn %= colCount;
                    }

                    if (CheckForTreeAtPosition(currentColumn, currentRow, inputLines) == true)
                    {
                        treeCount++;
                    }
                }
            }

            return treeCount.ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            int rowCount = inputLines.Length;
            int colCount = inputLines[0].Length;

            long answer = 1L;

            foreach (var offsets in JumpOffsetsB)
            {
                int treeCount = 0;

                int currentColumn = 0;
                for (int currentRow = offsets.down; currentRow < rowCount; currentRow += offsets.down)
                {
                    currentColumn += offsets.right;

                    if (currentColumn > (colCount - 1))
                    {
                        currentColumn %= colCount;
                    }

                    if (CheckForTreeAtPosition(currentColumn, currentRow, inputLines) == true)
                    {
                        treeCount++;
                    }
                }

                answer *= treeCount;
            }

            return answer.ToString();
        }
    }
}

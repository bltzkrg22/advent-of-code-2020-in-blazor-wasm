﻿using System.Threading.Tasks;

namespace Aoc2020_Blazor.Solutions
{
    public interface ISolution
    {
        Task<string> PartAAsync(string input);

        Task<string> PartBAsync(string input);
    }
}

﻿using System;
using System.Collections.Generic;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution10 : SolutionCommon
    {
        /*
        * This solution is written under assumption that all adapters 
        * have unique joltage. There is no validation for that!
        */
        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var adapterJoltageList = new List<int>();

            foreach (var line in inputLines)
            {
                adapterJoltageList.Add(Int32.Parse(line));
            }
            adapterJoltageList.Sort();

            /* 
            * Explnanation of "? 1 : 0" - if the first adapter has joltage 1, there is
            * a difference of 1 between this adapter and the outlet (which has joltage 0)
            * Analogically, if the first adapter has joltage 2 or 3, we add one to the
            * appropriate counter.
            * 
            * Also, there is a guaranteed difference of 3 between the final adapter and
            * the device! We also include it.
            */
            int countOneDiffs = adapterJoltageList[0] == 1 ? 1 : 0;
            // int countTwoDiffs = 0; // We don't care about twos
            int countThreeDiffs = (adapterJoltageList[0] == 3 ? 1 : 0) + 1;

            for (int i = 1; i < adapterJoltageList.Count; i++)
            {
                switch ((adapterJoltageList[i] - adapterJoltageList[i - 1]))
                {
                    case 1:
                        countOneDiffs++;
                        break;
                    case 2:
                        // countTwoDiffs++; // We don't care about twos
                        break;
                    case 3:
                        countThreeDiffs++;
                        break;
                    default:
                        throw new ApplicationException("Default case should be unreachable with valid input!");
                }
            }

            return (countOneDiffs * countThreeDiffs).ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var adapterJoltageList = new List<int>();

            foreach (var line in inputLines)
            {
                adapterJoltageList.Add(Int32.Parse(line));
            }
            adapterJoltageList.Sort();

            // To calculate the number of paths for a given adapter, we only need to know
            // the number of paths for three previous adapters.
            var previousThree = new Queue<(int joltage, long paths)>();

            /*
            * The trick below allows us to start the for loop without calculating
            * the first three dynamic variables manually. (0, 0) is a dummy element.
            * We need to provide (0, 1) exactly once, so that if the first adapter
            * has joltage 1, 2 or 3, we will correctly cacluate that there is exactly
            * one path from outlet to the first adapter.
            */
            previousThree.Enqueue((0, 0));
            previousThree.Enqueue((0, 0));
            previousThree.Enqueue((0, 1));

            for (int i = 0; i < adapterJoltageList.Count; i++)
            {
                int currentJoltage = adapterJoltageList[i];
                long currentPaths = 0;

                foreach (var previous in previousThree)
                {
                    if (currentJoltage - previous.joltage <= 3)
                    {
                        currentPaths += previous.paths;
                    }
                }

                _ = previousThree.Dequeue();
                previousThree.Enqueue((currentJoltage, currentPaths));
            }

            _ = previousThree.Dequeue();
            _ = previousThree.Dequeue();
            (_, long finalAdapterPaths) = previousThree.Dequeue();

            /*
            * The difference between final adapter and the other end is always assumed
            * to be three, so there is only one path from the final adapter - i.e.
            * total number of paths == number of paths including the final adapter!
            */
            return finalAdapterPaths.ToString();
        }
    }
}

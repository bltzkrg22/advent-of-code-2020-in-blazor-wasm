﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution20 : SolutionCommon
    {
        //private const char WhitePixel = '.';
        private const char BlackPixel = '#';

        private int JigsawSideLength;

        private Dictionary<int, JigsawPiece> tiles;
        private int[] OrderOfTiles;

        // Given a 2D matrix, which we can rotate or mirror, there are in general
        // 8 possible configurations that the result 2D matrix can have. Please consult
        // the problem description to see those configurations.
        private static (int X, int Y) GetOrientedBit(int x, int y, int orientation, int sideSize) 
            => orientation switch
            {
                0 => (x, y),
                1 => (sideSize - 1 - y, x),
                2 => (sideSize - 1 - x, sideSize - 1 - y),
                3 => (y, sideSize - 1 - x),
                4 => (sideSize - 1 - x, y),
                5 => (sideSize - 1 - y, sideSize - 1 - x),
                6 => (x, sideSize - 1 - y),
                7 => (y, x),
                _ => (-1, -1)
            };

        private int CalculateOrientation(int top, int left) => (top, left) switch
        {
            (0, 3) => 0,
            (1, 0) => 1,
            (2, 1) => 2,
            (3, 2) => 3,
            (0, 1) => 4,
            (1, 2) => 5,
            (2, 3) => 6,
            (3, 0) => 7,
            _ => -1
        };

        private static int BorderClass(int border, int bitsize)
        {
            int borderCopy = border;
            int flipped = 0;

            for (int i = 0; i < bitsize; i++)
            {
                flipped <<= 1; // bit sgifting must be done before adding the digit
                               // otherwise you can't get 1 as the least significant bit! 

                int digit = borderCopy & 1;

                flipped += digit;
                borderCopy >>= 1;
            }

            return Math.Min(border, flipped);
        }





        private class JigsawPiece
        {
            private int ContentSize { get; set; }
            public int ID { get; }
            public int[] Border { get; private set; }
            public char[,] Contents { get; private set; }
            public int Orientation { get; set; }


            public JigsawPiece(string inputData)
            {
                string[] rows = inputData.Split("\n",
                    StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);

                int rawSize = rows[1].Length;
                this.ContentSize = rawSize - 2;

                string titlePattern = @"^Tile (\d\d\d\d):$";
                var match = Regex.Match(rows[0], titlePattern);
                ID = Int32.Parse(match.Groups[1].Value);

                string borderTop = rows[1];

                var borderBottomSb = new StringBuilder(borderTop.Length);
                for (int i = 0; i < rawSize; i++)
                {
                    borderBottomSb.Insert(0, rows[rawSize][i]);
                }
                string borderBottom = borderBottomSb.ToString();



                var borderLeftSb = new StringBuilder(borderTop.Length);
                var borderRightSb = new StringBuilder(borderTop.Length);
                for (int i = 1; i <= rawSize; i++)
                {
                    borderLeftSb.Insert(0, rows[i][0]);
                    borderRightSb.Append(rows[i][9]);
                }
                string borderLeft = borderLeftSb.ToString();
                string borderRight = borderRightSb.ToString();

                // CSS margin order is: top right bottom left

                Border = new int[4];

                this.Border[0] = BorderToBinary(borderTop);
                this.Border[1] = BorderToBinary(borderRight);
                this.Border[2] = BorderToBinary(borderBottom);
                this.Border[3] = BorderToBinary(borderLeft);

                this.Contents = new char[ContentSize, ContentSize];

                for (int y = 2; y < 2 + ContentSize; y++)
                {
                    for (int x = 1; x < 1 + ContentSize; x++)
                    {
                        Contents[x - 1, y - 2] = rows[y][x];
                    }
                }

                Orientation = 0;
            }

            private static int BorderToBinary(string border)
            {
                int currentDigit = 1;
                int answer = 0;
                int borderLength = border.Length;

                for (int i = 0; i < borderLength; i++)
                {
                    if (border[borderLength - i - 1] == BlackPixel)
                    {
                        answer += currentDigit;
                    }
                    currentDigit <<= 1;
                }
                return answer;
            }

            public int RightTileEdge() => Border[CalculateRight(Orientation)];

            public int BottomTileEdge() => Border[CalculateDown(Orientation)];

            public char GetJigsawOrientedBit(int x, int y)
            {
                (int jx, int jy) = GetOrientedBit(x, y, Orientation, ContentSize);
                return Contents[jx, jy];
            }





            static int CalculateRight(int orientation) => orientation switch
            {
                0 => 1,
                1 => 2,
                2 => 3,
                3 => 0,
                4 => 3,
                5 => 0,
                6 => 1,
                7 => 2,
                _ => -1
            };

            static int CalculateDown(int orientation) => orientation switch
            {
                0 => 2,
                1 => 3,
                2 => 0,
                3 => 1,
                4 => 2,
                5 => 3,
                6 => 0,
                7 => 1,
                _ => -1
            };


        }




        public override string PartA(string input)
        {
            var borderClassDict = new Dictionary<int, List<int>>();
            tiles = new Dictionary<int, JigsawPiece>();

            string[] rawTiles = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            foreach (var tiledata in rawTiles)
            {
                var currentJigsaw = new JigsawPiece(tiledata);
                tiles[currentJigsaw.ID] = currentJigsaw;
            }

            // Number of pixels in the contents of a single tile. This does not
            // include the border pixels, which are discared later!
            int tileContentLength = tiles.First().Value.Contents.GetLength(0);
            int tileBorderLength = tileContentLength + 2;

            // This array will map position in the final jigsaw to tileId
            int[] orderOfTiles = Enumerable.Repeat(-1, tiles.Count).ToArray();

            // Square the number of tiles (jigsaw pieces) to calculate the number of tiles
            // that fit in a single row/column of the final jigsaw
            int side = 0;
            while (side * side < tiles.Count)
            {
                side++;
            }
            JigsawSideLength = side;

            foreach (var tile in tiles)
            {
                // Each tile has four sides, that's why we use a magic number here
                for (int i = 0; i < 4; i++)
                {
                    if (!borderClassDict.ContainsKey(BorderClass(tile.Value.Border[i], tileBorderLength)))
                    {
                        borderClassDict[BorderClass(tile.Value.Border[i], tileBorderLength)] = new List<int>();
                    }
                    borderClassDict[BorderClass(tile.Value.Border[i], tileBorderLength)].Add(tile.Value.ID);
                }
            }

            #region FindTopLeftTile

            bool cornerFound = false;
            int cornerTileID;

            foreach (var tile in tiles)
            {
                if (cornerFound)
                {
                    break;
                }
                cornerTileID = tile.Key;
                int i = 0;

                HashSet<int> unpairedEdges = new HashSet<int>();

                while (unpairedEdges.Count < 2 && i < 4)
                {
                    if (borderClassDict[BorderClass(tile.Value.Border[i], tileBorderLength)].Count == 1)
                    {
                        unpairedEdges.Add(i);
                    }
                    i++;
                }

                if (unpairedEdges.Count == 2)
                {
                    cornerFound = true;
                    orderOfTiles[0] = tile.Key;

                    if (unpairedEdges.Contains(3) && unpairedEdges.Contains(0))
                    {
                        tile.Value.Orientation = 0;
                    }
                    else if (unpairedEdges.Contains(0) && unpairedEdges.Contains(1))
                    {
                        tile.Value.Orientation = 1;
                    }
                    else if (unpairedEdges.Contains(1) && unpairedEdges.Contains(2))
                    {
                        tile.Value.Orientation = 2;
                    }
                    else
                    {
                        tile.Value.Orientation = 3;
                    }
                }
            }

            if (cornerFound == false)
            {
                throw new ApplicationException("Input data is incorrect.");
            }

            #endregion


            #region FindTopRow

            for (int i = 1; i < side; i++)
            {
                int tileToLeft = orderOfTiles[i - 1];


                int commonVertBorderClass = BorderClass(tiles[tileToLeft].RightTileEdge(), tileBorderLength);

                if (tileToLeft == borderClassDict[commonVertBorderClass][0])
                {
                    orderOfTiles[i] = borderClassDict[commonVertBorderClass][1];
                }
                else
                {
                    orderOfTiles[i] = borderClassDict[commonVertBorderClass][0];
                }

                int newfoundTile = orderOfTiles[i];

                // Find, which border tiles have in common, and return the index in the newfound tile
                int commonVertIndex = -1;
                for (int j = 0; j <= 3; j++)
                {
                    if (BorderClass(tiles[newfoundTile].Border[j], tileBorderLength) == commonVertBorderClass)
                    {
                        commonVertIndex = j;
                        break;
                    }
                }

                // Find, which border from set {j-1, j+1} is unpaired (this will be the top edge_

                int jm1 = (4 + commonVertIndex - 1) % 4;    // wrap values to {0,1,2,3}
                int jp1 = (commonVertIndex + 1) % 4;        // wrap values to {0,1,2,3}

                int xm1 = BorderClass(tiles[newfoundTile].Border[jm1], tileBorderLength);
                int xp1 = BorderClass(tiles[newfoundTile].Border[jp1], tileBorderLength);

                if (borderClassDict[xm1].Count == 1)
                {
                    tiles[newfoundTile].Orientation = CalculateOrientation(jm1, commonVertIndex);
                }
                else if (borderClassDict[xp1].Count == 1)
                {
                    tiles[newfoundTile].Orientation = CalculateOrientation(jp1, commonVertIndex);
                }
                else
                {
                    throw new ApplicationException("Input data is incorrect.");
                }
            }

            #endregion


            #region FindLeftRow

            for (int i = 1; i < side; i++)
            {
                int tileToTop = orderOfTiles[(i - 1) * side];


                int commonHorzBorderClass = BorderClass(tiles[tileToTop].BottomTileEdge(), tileBorderLength);

                if (tileToTop == borderClassDict[commonHorzBorderClass][0])
                {
                    orderOfTiles[i * side] = borderClassDict[commonHorzBorderClass][1];
                }
                else
                {
                    orderOfTiles[i * side] = borderClassDict[commonHorzBorderClass][0];
                }

                int newfoundTile = orderOfTiles[i * side];

                // Find, which border tiles have in common, and return the index in the newfound tile

                int commonHorzIndex = -1;
                for (int j = 0; j <= 3; j++)
                {
                    if (BorderClass(tiles[newfoundTile].Border[j], tileBorderLength) == commonHorzBorderClass)
                    {
                        commonHorzIndex = j;
                        break;
                    }
                }

                // Find, which border from set {j-1, j+1} is unpaired (this will be the LEFT edge)

                int jm1 = (4 + commonHorzIndex - 1) % 4;    // wrap values to {0,1,2,3}
                int jp1 = (commonHorzIndex + 1) % 4;        // wrap values to {0,1,2,3}

                int xm1 = BorderClass(tiles[newfoundTile].Border[jm1], tileBorderLength);
                int xp1 = BorderClass(tiles[newfoundTile].Border[jp1], tileBorderLength);

                if (borderClassDict[xm1].Count == 1)
                {
                    tiles[newfoundTile].Orientation = CalculateOrientation(commonHorzIndex, jm1);
                }
                else if (borderClassDict[xp1].Count == 1)
                {
                    tiles[newfoundTile].Orientation = CalculateOrientation(commonHorzIndex, jp1);
                }
                else
                {
                    throw new ApplicationException("Input data is incorrect.");
                }
            }

            #endregion


            #region FindTheRest

            for (int j = 1; j < side; j++)
            {
                for (int i = 1; i < side; i++)
                {
                    int currentPos = i + j * side;
                    int tileToLeft = orderOfTiles[currentPos - 1];
                    int tileToTop = orderOfTiles[currentPos - side];

                    int commonVertBorderClass = BorderClass(tiles[tileToLeft].RightTileEdge(), tileBorderLength);
                    int commonHorzBorderClass = BorderClass(tiles[tileToTop].BottomTileEdge(), tileBorderLength);

                    if (tileToLeft == borderClassDict[commonVertBorderClass][0])
                    {
                        orderOfTiles[currentPos] = borderClassDict[commonVertBorderClass][1];
                    }
                    else
                    {
                        orderOfTiles[currentPos] = borderClassDict[commonVertBorderClass][0];
                    }

                    int newfoundTile = orderOfTiles[currentPos];

                    // Find, which border tiles have in common, and return the index in the newfound tile

                    int commonVertIndex = -1;
                    for (int k = 0; k <= 3; k++)
                    {
                        if (BorderClass(tiles[newfoundTile].Border[k], tileBorderLength) == commonVertBorderClass)
                        {
                            commonVertIndex = k;
                            break;
                        }
                    }

                    // Find, which border from set {j-1, j+1} is matched with tile directly over (this will be the TOP edge)

                    int commonHorzIndex = -1;
                    for (int k = 0; k <= 3; k++)
                    {
                        if (BorderClass(tiles[newfoundTile].Border[k], tileBorderLength) == commonHorzBorderClass)
                        {
                            commonHorzIndex = k;
                            break;
                        }
                    }
                    tiles[newfoundTile].Orientation = CalculateOrientation(commonHorzIndex, commonVertIndex);
                }
            }

            #endregion


            OrderOfTiles = orderOfTiles;

            long cornerProd = 1L;

            cornerProd *= orderOfTiles[0];
            cornerProd *= orderOfTiles[side - 1];
            cornerProd *= orderOfTiles[side * (side - 1)];
            cornerProd *= orderOfTiles[side * side - 1];



            return cornerProd.ToString();
        }

        public override string PartB(string input)
        {
            // We invoke PartA to solve the jigsaw and create a big image
            _ = PartA(input);

            int numberOfMonsters = 0;
            bool monsterFound = false;

            int tileContentLength = tiles.First().Value.Contents.GetLength(0);

            int nessieRegionWidth = 20;
            int nessieRegionHeight = 3;

            for (int o = 0; o < 7; o++) // for each bigOrientation
            {
                for (int x = 0; x < JigsawSideLength * tileContentLength - nessieRegionWidth; x++)
                {
                    for (int y = 0; y < JigsawSideLength * tileContentLength - nessieRegionHeight; y++)
                    {
                        char[,] regionToCheck = new char[nessieRegionWidth, nessieRegionHeight];

                        for (int col = 0; col < nessieRegionWidth; col++)
                        {
                            for (int row = 0; row < nessieRegionHeight; row++)
                            {
                                regionToCheck[col, row] = AccessImageBit(x + col, y + row, tiles, OrderOfTiles, o);
                            }
                        }

                        if (CheckRegionForNessie(regionToCheck))
                        {
                            monsterFound = true;
                            numberOfMonsters++;
                        }
                    }
                }

                // While this is not specified in problem description, the monsters can be found only in
                // a single possible orientation. Therefore, if we found a monster in current orientation,
                // we don't need to check the other orientations anymore.
                if (monsterFound)
                {
                    break;
                }
            }



            int blackPixelCounter = 0;
            for (int x = 0; x < JigsawSideLength * tileContentLength; x++)
            {
                for (int y = 0; y < JigsawSideLength * tileContentLength; y++)
                {
                    if (AccessImageBit(x, y, tiles, OrderOfTiles, 0) == BlackPixel)
                    {
                        blackPixelCounter++;
                    }
                }
            }

            // We assume that the monsters do not overlap!
            return (blackPixelCounter - 15 * numberOfMonsters).ToString();


            char AccessImageBit(int imageX, int imageY, Dictionary<int, JigsawPiece> tiles, int[] orderOfTiles, int bigOrientation)
            {
                // Contents of a single tile form a square, so we check only one dimension
                int tileContentSize = tiles[orderOfTiles[0]].Contents.GetLength(0);

                int tileNumberSqrt = JigsawSideLength;

                (int orientedX, int orientedY) = GetOrientedBit(imageX, imageY, bigOrientation, tileContentSize * tileNumberSqrt);

                int tileIndex = (orientedX / tileContentSize) + tileNumberSqrt * (orientedY / tileContentSize);

                int tileIDAtCoords = orderOfTiles[tileIndex];

                return tiles[tileIDAtCoords].GetJigsawOrientedBit(orientedX % tileContentSize, orientedY % tileContentSize);
            }


            bool CheckRegionForNessie(char[,] region)
            {
                //  01234567890123456789
                //0 ^                 # 
                //1 #    ##    ##    ###
                //2  #  #  #  #  #  #   

                //^ is the top left corner of the 20×3 region to check

                (int, int)[] offsets = { (0, 1), (1, 2), (4, 2), (5, 1), (6, 1), (7, 2),
                                         (10, 2), (11, 1), (12, 1), (13, 2), (16, 2),
                                         (17, 1), (18, 0), (18, 1), (19, 1) };

                bool checkError = false;

                foreach (var offset in offsets)
                {
                    if (region[offset.Item1, offset.Item2] != BlackPixel)
                    {
                        checkError = true;
                        break;
                    }
                }

                return !checkError;
            }
        }
    }
}

﻿using System;
using System.Linq;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution15 : SolutionCommon
    {
        private const int PartAIterations = 2020;
        private const int PartBIterations = 30_000_000;
        private string PartCommon(string input, int iterations)
        {
            string[] initialNumbers = input.Split(",",
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var lastRecitation = new int[iterations + 1];
            for (int i = 0; i < lastRecitation.Length; i++)
            {
                lastRecitation[i] = -1;
            }

            for (int i = 0; i < initialNumbers.Length - 1; i++)
            {
                lastRecitation[Int32.Parse(initialNumbers[i])] = i;
            }

            int currentNumberRecited = Int32.Parse(initialNumbers.Last());


            for (int i = initialNumbers.Length - 1; i <= iterations - 2; i++)
            {
                currentNumberRecited = RecitedNumberAge(currentNumberRecited, i, lastRecitation);
            }

            return currentNumberRecited.ToString();

            int RecitedNumberAge(int numberToLook, int currentIndex, int[] arrayToSearch)
            {
                int memoryContent = arrayToSearch[numberToLook];
                arrayToSearch[numberToLook] = currentIndex;

                return memoryContent != -1 ? currentIndex - memoryContent : 0;
            }
        }

        public override string PartA(string input) => PartCommon(input, PartAIterations);

        public override string PartB(string input) => PartCommon(input, PartBIterations);

    }
}

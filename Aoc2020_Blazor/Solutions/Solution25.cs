﻿using System;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution25 : SolutionCommon
    {
        private const int Prime = 20_201_227;
        private const int SubjectNumber = 7;

        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            int doorPublicKey = Int32.Parse(inputLines[0]);
            int keycardPublicKey = Int32.Parse(inputLines[1]);

            int doorLoopSize = FindLoopSize(doorPublicKey);
            // Computing keycardLoopSize is unnecessary
            //int keycardLoopSize = FindLoopSize(keycardPublicKey);

            int encryptionKey = Transform(keycardPublicKey, doorLoopSize);

            return encryptionKey.ToString();


            int FindLoopSize(int publicKey)
            {
                int loopSize = 0;
                long value = 1L;

                // p = 20201227 is a prime
                // therefore for every n that is coprime with p (which includes 7)
                // 1, n, n^2, n^3, ..., n^(p-1)
                // have all different remainders modulo p

                bool success = false;
                while (!success)
                {
                    loopSize++;
                    value *= SubjectNumber;
                    value %= Prime;

                    if (value == publicKey)
                    {
                        success = true;
                    }
                }

                return loopSize;
            }


            int Transform(int subject, int loopSize)
            {
                long value = 1L;

                for (int i = 0; i < loopSize; i++)
                {
                    value *= subject;
                    value %= Prime;
                }

                return (int)value;
            }

        }

        public override string PartB(string input) => "No Part B on day 25!";
    }
}

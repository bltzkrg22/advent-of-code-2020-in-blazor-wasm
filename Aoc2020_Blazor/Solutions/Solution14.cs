﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution14 : SolutionCommon
    {
        string memAddressPattern = @"mem\[(\d+)\] = (\d+)";
        string maskPattern = @"mask = ([01X]+)";

        private long BitReplacerA(long number, string mask)
        {
            long currentBit = 1;
            long currentNumber = number;
            int maskLength = mask.Length;

            for (int i = 0; i < maskLength; i++)
            {
                char maskBit = mask[maskLength - i - 1];

                switch (maskBit)
                {
                    case '0':
                        // (Any bit) & 1 = (Initial bit); (Any bit) & 0 = 0
                        // So here we change only currentBit to 0
                        // Noitce the negation: ~currentBit
                        currentNumber &= (~currentBit); 
                        break;
                    case '1':
                        // (Any bit) | 0 = (Initial bit); (Any bit) | 1 = 1
                        // So here we change only currentBit to 1
                        currentNumber |= currentBit;
                        break;
                    case 'X':
                        // Do nothing
                        break;
                }

                currentBit <<= 1;
            }

            return currentNumber;
        }

        private List<long> AddressesToReplaceB(long address, string mask)
        {
            long currentBit = 1;
            long currentAddress = address;
            int maskLength = mask.Length;

            // In first pass we will overwrite those bits where mask has ones
            for (int i = 0; i < maskLength; i++)
            {
                char maskBit = mask[maskLength - i - 1];
                if (maskBit == '1')
                {
                    currentAddress |= currentBit;
                }
                currentBit <<= 1;
            }

            // In second pass, we will create all addresses considering 'X'-es in mask
            var answerAddresses = new List<long>() { currentAddress };

            currentBit = 1;
            for (int i = 0; i < maskLength; i++)
            {
                char maskBit = mask[maskLength - i - 1];
                if (maskBit == 'X')
                {
                    // Each 'X' duplicates the number of answers
                    int currentNumberOfAddresses = answerAddresses.Count;
                    for (int j = 0; j < currentNumberOfAddresses; j++)
                    {
                        long newAdress = answerAddresses[j] ^ currentBit;
                        answerAddresses.Add(newAdress);
                    }
                }
                currentBit <<= 1;
            }

            return answerAddresses;
        }


        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            // Values and memory addresses are both 36-bit unsigned integers, so we use long, long 
            var memoryContents = new Dictionary<long, long>();

            // "Neutral" mask for Part A
            string currentMask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            foreach (var line in inputLines)
            {
                if (line.StartsWith("mem"))
                {
                    var match = Regex.Match(line, memAddressPattern);

                    long address = Int64.Parse(match.Groups[1].Value);
                    long value = Int64.Parse(match.Groups[2].Value);
                    memoryContents[address] = BitReplacerA(value, currentMask);
                }
                else if (line.StartsWith("mask"))
                {
                    var match = Regex.Match(line, maskPattern);

                    currentMask = match.Groups[1].Value;
                }
            }

            long answer = memoryContents.Values.Sum();

            return answer.ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            // Values and memory addresses are both 36-bit unsigned integers, so we use long, long 
            var memoryContents = new Dictionary<long, long>();

            // "Neutral" mask for Part B - different that in Part A!
            string currentMask = "000000000000000000000000000000000000";
            foreach (var line in inputLines)
            {
                if (line.StartsWith("mem"))
                {
                    var match = Regex.Match(line, memAddressPattern);

                    long address = Int64.Parse(match.Groups[1].Value);
                    long value = Int64.Parse(match.Groups[2].Value);
                    foreach (var floatingAddress in AddressesToReplaceB(address, currentMask))
                    {
                        memoryContents[floatingAddress] = value;
                    }
                }
                else if (line.StartsWith("mask"))
                {
                    var match = Regex.Match(line, maskPattern);

                    currentMask = match.Groups[1].Value;
                }
            }

            long answer = memoryContents.Values.Sum();

            return answer.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aoc2020_Tests
{
    public interface ISolutionTests
    {
        public int Day { get; }
        public void PartATest(string input, string expectedAOutput, string expectedBOutput);
        public void PartBTest(string input, string expectedAOutput, string expectedBOutput);
    }
}

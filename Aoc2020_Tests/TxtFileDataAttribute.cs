﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Xunit.Sdk;

namespace xUnitHelpers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class TxtFileDataAttribute : DataAttribute
    {
        private readonly string _inFilePath;
        private readonly string _outFilePath;

        public TxtFileDataAttribute(string inFilePath, string outFilePath)
        {
            _inFilePath = inFilePath;
            _outFilePath = outFilePath;
        }


        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            if (testMethod == null)
            {
                throw new ArgumentNullException(nameof(testMethod));
            }

            // Test scenarios are copied to output directory before building
            string outputDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            string relativeInPath = _inFilePath;
            string fullInPath = Path.Join(outputDirectory, relativeInPath);
            if (!File.Exists(fullInPath))
            {
                throw new ArgumentException($"Could not find file at path: {fullInPath}");
            }

            string relativeOutPath = _outFilePath;
            string fullOutPath = Path.Join(outputDirectory, relativeOutPath);
            if (!File.Exists(fullOutPath))
            {
                throw new ArgumentException($"Could not find file at path: {fullOutPath}");
            }

            // Load the file
            var inFileData = File.ReadAllText(fullInPath);
            var outFileData = File.ReadAllLines(fullOutPath);

            //                  new object[] { input,      partAanswer,    partBanswer };
            object[] testCase = new object[] { inFileData, outFileData[0], outFileData[1] };

            return new List<object[]>() { testCase };
        }
    }
}
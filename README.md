# Advent of Code 2020 in Blazor WASM

A solver for the problems from [*Advent of Code 2020*](https://adventofcode.com/2020). It demonstrates the usage of Blazor WebAssembly (sometimes called Blazor Client) for building single page web applications.

While the *AoC* problems are not particularly hard, they *do* require some level of problem solving ability, data structure knowledge, etc.

## Additional information

Since the application is self-contained and requires no additional servies to be run, it is deployed on [Netlify](https://netlify.app/) and uses its continuous integration service to automatically build from GitLab.

### Build command

Using the following build command publishes only the Blazor app, i.e. exludes the test project (imporant especially for publishing on Netlify).

```
dotnet publish ./Aoc2020_Blazor/Aoc2020_Blazor.csproj -f net5.0 -c Release -o release
```
